<?php require './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP OOP Activity</title>
</head>
<body>
    <ul>
        <li><?php print_r($newProduct); ?></li>
        <li><?php print_r($newMobile); ?></li>
        <li><?php print_r($newComputer); ?></li>
    </ul>

    
    <?php $newProduct->setStockNo(3); ?>
    <?php $newMobile->setStockNo(5); ?>
    <?php $newComputer->setCategory("laptops, computers, and electronics"); ?>


    <ul>
        <li>Stock No : <?php echo $newProduct->getStockNo(); ?></li>
        <li>Mobile Stock No : <?php echo $newMobile->getStockNo(); ?></li>
        <li>Computer Description : <?php echo $newComputer->getCategory(); ?></li>
    </ul>
    
</body>
</html>