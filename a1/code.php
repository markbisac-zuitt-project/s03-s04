<?php 
    class Product{
        public $name;
        public $price;
        public $description;
        public $category;
        public $stockNum;

        public function __construct($pName, $pPrice, $pDes, $pcategory, $pStock){
            $this->name = $pName;
            $this->price = $pPrice;
            $this->description =$pPrice;
            $this->category = $pcategory;
            $this->stockNum = $pStock;
        }

        public function printProdDetails(){
            return "The product has a name of $this->name and its price is $this->price, and the stock no is $this->stockNum";
        }

        // Getters
        public function getPrice(){
           return  $this->price;
        }

        public function getStockNo(){
           return $this->stockNum;
        }

        public function getCategory(){
           return $this->category;
        }


        // Setters
        public function setPrice($priceValue){
           return $this->price = $priceValue;
        }

        public function setStockNo($stockValue){
           return $this->stockNum = $stockValue;
        }

        public function setCategory($catValue){
           return $this->category = $catValue;
        }
      
    }

    $newProduct = new Product("Xioami Mi Monitor", 22000.00, "Good for gaming and for coding", "computer-peripherals", 5);

    class Mobile extends Product{

        public function printMobDetails(){
            return "Our latest mobile is $this->name with a cheapest price of $this->price";
        }
    }

    $newMobile = new Mobile("Xiami Redmi Note 10 Pro", 13590.00 , "Lates Xiamo Phone ever made", "mobiles and electronics", 10);

    class Computer extends Product{
        public function printComDetails(){
            return "Our newst Computer is $this->name and its base price is $this->price";
        }
    }

    $newComputer = new Computer("HP Business Laptop", 29000.00 , "HP Laptop only made for business", "laptops and computer", 10);


?>