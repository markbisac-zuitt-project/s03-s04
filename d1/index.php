<?php require './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Classes and Objects</title>
</head>
<body>
    <h1>PHP Classes and Objects</h1>
    <?php echo $newCondoUnit->getName(); ?>
    <pre>
        <?php 
            print_r($buildingObj);
        ?>
    </pre>

    <pre>
        <?php print_r($newBuilding); ?>
    </pre>

    <pre>
        <?php print_r($newPerson); ?>
    </pre>

    <p>
        <?php echo $newBuilding->getBuildingDetails(); ?>
    </p>

    <?php
        $newBuilding->setFloors(6);
    ?>

    <ul>
        <li>
           Building Name : <?php echo $newBuilding->getName(); ?>
        </li>
        <li>
           Floor Number : <?php echo $newBuilding->getFloors(); ?>
        </li>
        <li>
           Address : <?php echo $newBuilding->getAddress(); ?>
        </li>
    </ul>

    
    <?php $newPerson->setFirstName("Max"); ?>
    <?php $newPerson->setMiddleName("Tae"); ?>
    <?php $newPerson->setLastName("Young"); ?>


    <ul>
        <li><?php echo $newPerson->getFirstName(); ?></li>
        <li><?php echo $newPerson->getMiddleName(); ?></li>
        <li><?php echo $newPerson->getLastName(); ?></li>
    </ul>

    <hr>
    <p>
        <?php echo $newCondoUnit->getBuildingDetails();?>
    </p>
  
</body>
</html>