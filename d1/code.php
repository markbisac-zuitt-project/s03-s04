<?php

    $buildingObj = (object)[
        'name' => 'Caswyn Bldg',
        'floors' => 8,
        'address' => (object)[
            'barangay' => 'Sacred Heart',
            'city' => 'Ilocos',
            'country' => 'Philippines'
        ]
    ];

    // Creating a Blueprint/base class of our building
    class Building {
        // public keyowrd - visibility level of how the methods/properties can be accessed by the outsiders
        // protected - allows the childre/subclassor itself to access and inherit  its method and properties, however it will ban the private outsiders
        // private - disables the direct access and also the inheritance of the classes

        // protected uses for common properties, in order subclasses no direct access
        // private use for passwords, token
        public $name;
        public $floors;
        public $address;

        // Construct Functions
        public function __construct($nameValue, $floorsValue, $addressValue){
            $this->name = $nameValue;
            $this->floors = $floorsValue;
            $this->address = $addressValue;
        }

      
        // encapsulate the process and at same time these are the getters and setters
        public function getBuildingDetails(){
            return "$this->name, $this->floors and $this->address";
        }

        public function getName(){//getter
            return $this->name;
        }

        public function getFloors(){
            return $this->floors;
        }

        public function getAddress(){
            return $this->address;
        }

        public function setFloors($floorsValue){
            $this->floors = $floorsValue;
        }
    }

    // instatiate an object
    $newBuilding = new Building('Daniel Bldg', 5, 'Buendia Avenue, Makati City, Philippines');
    // $newBuilding->getFloors();

    //inherits the properties/methods of the base class building
    // Inheritance
    class Condominium extends Building{
        // Polymorphism
        public function getBuildingDetails(){
            return "$this->name, $this->floors and $this->address";
        }
    }

    $newCondoUnit = new Condominium('Avida Land', 120, 'Taguig, Philippines');


    class Person {
        public $firstName;
        public $middleName;
        public $lastName;
        public $age;
        public $birthday;

        public function __construct($fValue, $mValue, $lValue, $aValue, $bValue)
        {
            $this->firstName = $fValue;
            $this->middleName = $mValue;
            $this->lastName = $lValue;
            $this->age = $aValue;
            $this->birthday = $bValue;
        }

      public  function getFirstName(){
           return $this->firstName;
        }

      public  function getMiddleName(){
           return $this->middleName;
        }

      public  function getLastName(){
           return $this->lastName;
        }

        public function setfirstName($fName){
           $this->firstName = $fName;
        }

        public function setLastName($lName){
            $this->lastName = $lName;
        }

        public function setMiddleName($mName){
            $this->middleName = $mName;
        }

    }

    $newPerson = new Person('Mark', 'Daniel', 'Bisac', 20, '06-04-2001');

?>